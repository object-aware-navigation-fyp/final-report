\chapter{Self-appraisal}\label{appraisal_chapt}

\section{Achievements}
The aim of the project, to establish object-awareness in mobile robots enabling them to create new paths in the world, has been met, and
all of the minimum objectives set out in Chapter \ref{intro_chapt} were completed on time and rigorously tested. Of the three optional objectives specified, YOLACT and "Search and Fetch" were provided; real-world testing could not be performed due to circumstances in Appendix \ref{pandemic}.

\bigskip

Milestones were achieved in object awareness and assistive robotics functionality throughout the project. Experiments in Chapter \ref{exp_chapt} demonstrate the system's ability to navigate paths deemed unnegotiable by available autonomous navigation systems, while also showing its applicability to perform benchmark service tasks in international robotics competitions.

\bigskip

The proposed software was also seen to address challenges faced by previous Final Year Undergraduate Projects, such as the autonomous selection of points to search for and interact with objects \cite{momo, praveen} in Experiment \ref{exp_sect_search} and following targets around corners in Experiment \ref{exp_sect_follow}.



\section{Challenges}
As explained in Chapter \ref{research_chapt}, no academic readings were found that directly addressed the issues identified for this project, significantly increasing the risk of project failure as there were few related works to base developments and successes on.
This risk was mitigated by breaking down the project into sub-problems in the widely researched fields of Instance Segmentation, Multiple Object Tracking and Semantic Mapping. This meant benchmarking tools and experiments from other fields could be leveraged to test the project, while also ensuring the development of useful packages that would forward the research regardless of the outcomes of integration testing.

\bigskip

The TIAGo platform's software distribution supported Ubuntu 16.04, ROS Kinetic and Python 2.7 among other libraries used in the project. Many of the technologies were approaching End of Life, resulting in limited support for newer computer vision implementations explored in this project. In overcoming this challenge, I was able to learn useful skills in understanding and adapting open source software, backporting features from later versions of YOLOv3-PyTorch, YOLACT, AB3DMOT and \acrshort{pcl} for use in the project.

\bigskip
GPU acceleration for computer vision meant that software was not independent of underlying hardware architectures, running the risk of delivering non-portable code.
This limitation encouraged me to maintain a strong awareness of the end user throughout development, where many design choices were centred around ease of installation on new systems.
For example, PyTorch implementations were chosen or developed for all Deep Learning functionality in the project, as the framework requires minimal set-up to enable or disable GPU acceleration.



\section{Future Work}
\subsection{Research}\label{appraisal_sect_research}
As explained in Section \ref{eval_subsect_apps}, the proposed system requires user-defined logic at the planning level to navigate  obstructions effectively. Further research can be undertaken to bring the system closer to full autonomy, where Deep Reinforcement Learning has achieved relevant success exhibiting intelligent behaviour in video games and robotics \cite{rl_atari, rl_dota, rl_quadruped, rl_locomotion}.

\bigskip

The Object Aware Navigation system of this project can be replaced by a Deep Reinforcement Learning Neural Network to introduce a greater level of autonomy. At a coarse granularity, the network could learn to select between user-defined actions such as movement or manipulation towards reaching the navigation goal. At a finer granularity, the actions themselves could be learned by the network to autonomously move different objects out of the way.

\bigskip



\subsection{Optimisations}\label{appraisal_sect_optim}
Optimisations can be made to the 3D perception pipelines to target real-time performance.
Supervoxel clustering was identified to be the main bottleneck of the YOLOv3 perception pipeline as it did not scale with larger point clouds. Stein et al. \cite{lccp} state that this operation could be parallelised on a GPU for a 10x speedup.
On the other hand, migrating to ROS Noetic (Ubuntu 20.04) would enable full support of Python 3.8 towards integrating real-time instance segmentation through YOLACT and YOLACT++ \cite{yolact, yolact++}, without the communication overheads currently needed to interact with Python 2.7 for ROS Kinetic.

\bigskip

Modifications were made directly to the ROS Navigation Stack to enable Object Aware Navigation, although these changes can be refactored into plugins for the global planner and cost map.
This would allow for a significantly more modular design that would increase the ease of distribution, where users need only build the object-aware software plugins on top of their currently installed versions of the navigation stack.


\section{Personal Reflection}
In retrospect, I feel that I successfully achieved what I had set out to do in the project proposal, in part thanks to a comprehensive study on computer vision that allowed me to confidently identify key requirements of the solution. Good planning and adherence to the \acrshort{dsdm} software development model helped to transform my ideas into reality as I was able to consistently meet milestones and personal deadlines that kept the project on track.

\bigskip

The Object Aware Navigation system developed was proven to have a wide range of applications in autonomous service robotics, with its modular design enabling me to quickly implement the experiments that demonstrate this. I am excited to use the software to solve 3D perception in future robotics competitions and projects, in hopes of getting one step closer to fully autonomous service robots capable of effectively caring for the human race.