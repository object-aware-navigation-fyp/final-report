\chapter{Design}\label{design_chapt}
\section{System Architecture}
Object Aware Navigation was implemented as a stack, where lower-level modules use computer vision to build object awareness, while higher-level modules
perform mapping and planning to support navigation of the tracked objects.
A flowchart of the system is provided in Figure \ref{design_fig_sys_flowchart}.

\bigskip

All modules in the stack function independently of each other as standalone products.

\bigskip

\begin{figure}[ht]
	\centering	
	\includegraphics[width=\textwidth]{system_flowchart}
	\caption{Flowchart of the Object Aware Navigation stack}
	\label{design_fig_sys_flowchart}
\end{figure}



\section{3D Instance Segmentation Pipelines}
Two pipelines for object detection and 3D instance segmentation are proposed in Figure \ref{design_fig_lccp_flowchart} for implementation, making use of the popular YOLO and R-CNN object detection
frameworks as a base. Users are able to choose between them at launch based on preference.

\bigskip

\begin{figure}[ht]
	\centering	
	\includegraphics[scale=0.5]{pipelines_flowchart}
	\caption{Flowcharts of YOLOv3 (left) and Mask R-CNN (right)\\3D segmentation pipelines.}
	\label{design_fig_lccp_flowchart}
\end{figure}

%\begin{figure}[h]
%	\centering	
%	\includegraphics[scale=0.5]{lccp_flowchart}
%	\caption{Flowchart of the YOLOv3 pipeline}
%	\label{lccp_flowchart}
%\end{figure}
%
%\begin{figure}[h]
%	\centering	
%	\includegraphics[scale=0.5]{mask_rcnn_flowchart}
%	\caption{Flowchart of the Mask R-CNN pipeline}
%	\label{mask_rcnn_flowchart}
%\end{figure}


2D object detection frameworks were chosen as they are easier to train, require cheaper training data and are better at generalising
than the current state-of-the-art in 3D methods.
Two pipelines are proposed as Mask R-CNN affords higher accuracy at the cost of lower frame rates and expensive training data, while YOLOv3 offers higher performance and cheaper training data; limited to bounding boxes in place of instance segmentation masks. Users are thus able to decide between pipelines based on project requirements and budget.



\section{3D Multiple Object Tracking}
AB3DMOT was modified to store class information for each tracked object and implement a similarity heuristic inspired by Deep SORT.
We calculate the heuristic by projecting images of detected objects onto feature vectors and measuring the cosine distance between them, pairing this with 3D \acrshort{iou}
to perform maximum matching in the AB3DMOT algorithm.
\bigskip

This enables the system to be object-aware by tracking different classes in 3D space, while making it robust to 3D tracking errors by accepting additional 2D feature input for matching.



\section{Semantic Map}
The semantic map outputs an array of tracked 3D objects on a ROS topic such that the information can be used for a range of applications that require robot perception.
The \texttt{TrackedObject} class proposed to represent 3D tracked identities is depicted in Figure \ref{design_fig_tracked_obj_diagram}.

\bigskip

Additionally, an occupancy grid and image of the resulting semantic map are published to ROS topics for visualisation by users on RVIZ.

\bigskip

\begin{figure}[ht]
	\centering	
	\includegraphics[scale=0.6]{tracked_object}
	\caption{Initial Class Diagram of a tracked object}
	\label{design_fig_tracked_obj_diagram}
\end{figure}




\section{Object Aware Move Base}
The ROS Navigation stack's \texttt{costmap\_2d} was modified to set the cost of 3D tracked objects on the global costmap. This allows the mobile robot to plan through tracked
objects on the map where previously the cost would have been infinity. Local costmaps remain unaltered to avoid damaging collisions with objects or people.

\bigskip

\texttt{move\_base} was also expanded to create an \texttt{actionlib} client when the proposed plan on the costmap traverses through a tracked object. The client
sends a goal to an "Object Aware Handler" \texttt{actionlib} server implementing an action to handle the class of tracked object.

\bigskip

This creates a modular system where any number of handlers can be defined to navigate different classes of objects present in the semantic map, as shown in Figure \ref{design_fig_move_base_graph}.

\bigskip

\begin{figure}[ht]
	\centering	
	\includegraphics[scale=0.6]{move_base_graph}
	\caption{ROS computation graph of \texttt{move\_base}}
	\label{design_fig_move_base_graph}
\end{figure}


\section{Object Aware Handlers}
An Object Aware Handler was proposed for each of people and small objects, demonstrating the system's ability to autonomously clear paths for navigation by interacting with tracked objects.

\bigskip

The "person" handler interacts with people by asking them to move aside, using the \texttt{move\_base} Global Planner \cite{move_base} to wait for a path to open up. The "small object" handler directly removes obstructing objects from the path by picking them up with MoveIt.

\section{Experiments}
Six experiments were proposed to perform integration and acceptance testing during system development, targeting service robotics functionality for entry in international competitions \cite{erl_consumer, robocup_2020, sciroc} and the automation of labour intensive jobs in society.

\bigskip

The following experiments were proposed to test object permanence in the system:
\begin{itemize}
	\item Navigate to an object on the semantic map
	\item Count the instances of a class on the semantic map
	\item Search for and fetch an item in the environment
\end{itemize}

This experiment was proposed to test the system's 3D MOT capabilities:
\begin{itemize}
	\item Follow a person around corners
\end{itemize}

The following experiments were proposed to test object aware navigation:
\begin{itemize}
	\item Navigate a path blocked by a small object
	\item Navigate a path blocked by a person
\end{itemize}