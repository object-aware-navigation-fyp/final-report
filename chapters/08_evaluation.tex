\chapter{Evaluation}
\section{Technical Evaluation}
\subsection{Benchmarking}
Scores on the KITTI Object Tracking Evaluation benchmark (Table \ref{test_sect_kitti}) show increased average accuracy and precision in the proposed solution over AB3DMOT, but decreased maximum accuracy and precision with a higher occurrence of ID switching. As explained in Section \ref{test_sect_kitti}, the format of the data sets are not consistent with 2D object detection results, including labelled instances that are fully occluded. It is highly likely that we can expect an increase in performance when deploying the system with a 2D object detection framework, reflective of Deep SORT's success over the original SORT algorithm in the MOT Challenge \cite{deep_sort}.

\bigskip

Frame rate benchmarking in Section \ref{test_sect_framerate} indicates the system operating at means of 5.5 to 9.0 \acrshort{fps}. Qualitative analysis in Section \ref{eval_subsect_apps} shows this being sufficient for the purpose of Object Aware Navigation, however it is far from real-time speeds of 30 \acrshort{fps} and above. The main bottlenecks identified include ROS communication overheads and a lack of parallelisation in computer vision modules. Optimisations to solve these issues will be discussed in Section \ref{appraisal_sect_optim}.


\subsection{Experiments}

Semantic Mapping for Object Aware Navigation is constrained by the performance and accuracy of its underlying 3D perception and MOT implementations. This is seen in "Follow Person" in Section \ref{exp_sect_follow}, where the YOLACT-based pipeline failed to match the success of Mask R-CNN and YOLOv3 due to a lower quality of instance segmentation outputs for 3D tracking.

\bigskip

Despite such constraints, qualitative and quantitative results from the breadth of experiments conducted in Chapter \ref{exp_chapt} indicate a strong capability of the system to perform autonomous service and navigation tasks effectively using object-awareness.


\subsection{Delivery}
The Object Aware Move Base in Section \ref{impl_sect_oanav} was implemented in such a way that the currently selected version of the Navigation Stack would need to be replaced or deselected by the package manager. While this is handled effectively by the catkin package manager, the solution is subject to relatively longer build times and limited to specific versions of the Navigation Stack by design. A modular approach is described in Section \ref{appraisal_sect_optim} to solve this.

\bigskip

The use of experiments in Chapter \ref{exp_chapt} to perform integration testing meant that critical bugs could be discovered early during development while delivering useful scripts to perform autonomous service tasks. At the time of writing this report, no known bugs were present that had not been fixed.

\bigskip

Real-world testing could not be performed due to circumstances in Appendix \ref{pandemic}, however object detection and instance segmentation frameworks used in the project achieve high accuracy on real images \cite{yolov3,mask-rcnn,yolact,lccp}. Furthermore, 3D MOT benchmarking in Section \ref{test_sect_kitti} was performed on the KITTI\cite{kitti} dataset, featuring annotated images captured by cameras and laser sensors mounted on a station wagon driven along the roads of Karlsruhe, Germany \cite{kitti}. Good performance on computer vision benchmarks mitigates the risks of testing in simulation as the underlying perception modules are proven to work well in real-world situations.



\section{User Evaluation}
\subsection{Application}\label{eval_subsect_apps}
Experiments \ref{exp_sect_person} and \ref{exp_sect_object} demonstrate solutions to the navigation problem posed in Chapter \ref{intro_chapt}, while experiments \ref{exp_sect_goto} to \ref{exp_sect_follow} and \ref{exp_sect_search} show the system's adaptability to generalise to other autonomous services. Each experiment aims to solve a challenge posed in international scientific competitions \cite{erl_consumer, robocup_2020}, with specific use cases outlined for each task. This motivates the need and applicability of the system to solve real-world problems.

\bigskip

Despite failure to meet real time performance during benchmarking in Section \ref{test_sect_framerate}, the 3D perception pipeline is seen to operate sufficiently fast to create a map of encountered objects during navigation (Experiment \ref{exp_sect_count}) and follow moving targets with minimal susceptibility to ID switching (Experiment \ref{exp_sect_follow}).

\bigskip
 
While able to successfully clear obstructions to navigation, the proposed system is far from fully autonomous. Users are required to develop actions to interact with objects and specify namespaces that indicate when they are called. Future research in Section \ref{appraisal_sect_research} will discuss approaches to improve the system's autonomy.

\subsection{User Experience}
Implementations of experiments conducted in Chapter \ref{exp_chapt} evidence a positive user experience. Complex tasks such as following targets and object search are achieved in less than 100 lines of code using the high-level 3D perception interfaces offered by the project. Configuration files included with the project deliverables allow users to visualise the 3D processes in RVIZ.

\bigskip

The software was developed around usability, and as such the installation requirements were designed to target ROS Kinetic and the University's \acrfull{lasr} robotics team's technology stack. Libraries such as PyTorch and \acrshort{pcl} were chosen during implementation in Chapter \ref{impl_chapt} for ease of set up by new users. Full usage and installation instructions are included in the README of each package, viewable in Appendix \ref{readme_chapt}.