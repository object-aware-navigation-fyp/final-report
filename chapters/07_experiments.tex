\chapter{Experiments}\label{exp_chapt}
Six experiments were designed to test the applicability and ease of use of the developed software, based on real-world
problems and a number of scenarios posed in international robotics competitions.
Implementations of the experiments were kept decidedly simple to demonstrate the efficacy of the Object Aware Navigation system.

\bigskip

The experiments were also a means of integration and acceptance testing;
"Go To Object", "Count Objects", "Follow Person" and "Search and Fetch" test object detection, 3D MOT and Semantic Mapping and Navigation, while
"Navigate Through Person" and "Navigate Through Small Object" test Object Aware Navigation.

\section{Go To Object}\label{exp_sect_goto}
\textbf{Scenario:} The robot is driven through an unmapped room before navigating to an object.

\textbf{Use cases:} Care homes, workplace assistants, domestic robots.

\textbf{Description:} A basic example that tests object permanence by navigating to an object no longer in view.
Potential uses include fetching items for users with mobility issues.

\bigskip

\textbf{Implementation:} Search for an object of the input class name in the semantic map and use Semantic Navigation to approach it.

\bigskip

\textbf{Results:}
The robot successfully demonstrated object permanence by locating and navigating to objects encountered and no longer within view of the camera, shown in Table \ref{exp_tbl_goto}.
The system failed to track a coffee cup in one of the tests, where it was most likely not detected due to its relatively small size and thus filtered out by Algorithm \ref{impl_algo_deletion}.

\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|l|l|}
	\hline
	\textbf{Object} & \textbf{Number of Tests} & \textbf{Found} & \textbf{Approached} \\ \hline
	Person          & 5                        & 5              & 5                   \\ \hline
	Chair           & 5                        & 5              & 5                   \\ \hline
	Handbag         & 5                        & 5              & 5                   \\ \hline
	Coffee          & 5                        & 4              & 4                   \\ \hline
	\end{tabular}
	\caption{Table of results for "Go To Object".}
	\label{exp_tbl_goto}
\end{table}


\section{Count Objects}\label{exp_sect_count}
\textbf{Scenario:} The robot covers one lap of an unmapped café before counting the number of guests.

\textbf{Use cases:} Stock-taking, café waiter, receptionist

\textbf{Description:} A basic example that tests object permanence by counting objects no longer in view.
This problem was posed in the SciRoc Challenge 2019 \cite{sciroc}, and potential uses include stock-taking.
The set-up of the test café is depicted in Figure \ref{exp_fig_count}.

\bigskip

\textbf{Implementation:} Count the instances of a class in the semantic map.

\bigskip

\textbf{Results:} The robot accurately counted the number of people in the café in four out of five configurations, as shown in Table \ref{exp_tbl_count}. One false positive was detected at full capacity as the system failed to re-identify an occluded customer during navigation.
	\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|l|l|}
	\hline
	\textbf{Actual} & \textbf{Counted} & \textbf{True Positives} & \textbf{False Positives} \\ \hline
	1               & 1                & 1                       & 0                        \\ \hline
	9               & 9                & 9                       & 0                        \\ \hline
	15              & 15               & 15                      & 0                        \\ \hline
	21              & 21               & 21                      & 0                        \\ \hline
	28              & 29               & 28                      & 1                        \\ \hline
	\end{tabular}
	\caption{Table of results for "Count Objects".}
	\label{exp_tbl_count}
\end{table}

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{.6\textwidth}
		\includegraphics[width=\textwidth, trim=2350 100 50 200, clip]{cafe}
		\caption{Set up of the café scenario with 28 customers.}
	\end{subfigure}

	\bigskip

	\begin{subfigure}[b]{.6\textwidth}
		\includegraphics[width=\textwidth, trim=550 300 2610 350, clip]{cafe}
		\caption{Generated 3D semantic map after one lap of navigation.\\1 False Positive was detected.}
	\end{subfigure}
	\caption{"Count Objects" Experiment.}
	\label{exp_fig_count}
\end{figure}

\section{Follow Person}\label{exp_sect_follow}
\textbf{Scenario:} The robot follows a person around corners in an unmapped environment.

\textbf{Use cases:} Personal assistant, porter

\textbf{Description:} A benchmark functionality that tests 3D MOT, posed in the RoboCup@Home "Carry My Luggage" task.
Uses include robot assistants carrying heavy items for users.
The set-up of the test world is described in Figure \ref{exp_fig_follow}.


\begin{figure}[ht]
	\centering
	\includegraphics[width=.7\textwidth, trim=400 100 400 200, clip]{follow}
	\caption{"Follow Person" Experiment. The goal is to follow the person in front of the robot to the exit. Two people are placed near doorways to test robustness to False Targets.}
	\label{exp_fig_follow}
\end{figure}


\textbf{Implementation:} Semantic Navigation is used to repeatedly navigate to and look towards a tracked identity
on the semantic map.

\bigskip

\textbf{Results:} Semantic Mapping retained its underlying object tracking capabilities, successfully following persons to the goal with 0 ID switches in two configurations (Table \ref{exp_tbl_follow}). At no point did the robot follow the wrong person or object (False Target). Tests could not be completed using YOLACT due to frequent ID switching resulting from inaccurate segmentation masks.

\begin{table}[ht]
\centering
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{Pipeline} & \textbf{Use Encoder} & \textbf{ID Switches} & \textbf{False Targets} \\ \hline
Mask R-CNN        & N                     & 1                    & 0                      \\ \hline
YOLOv3 + LCCP     & N                     & 2                    & 0                      \\ \hline
YOLACT            & N                     & 3                    & 0                      \\ \hline
Mask R-CNN        & Y                     & \textbf{0}           & 0		              \\ \hline
YOLOv3 + LCCP     & Y                     & \textbf{0}           & 0		              \\ \hline
YOLACT            & Y                     & Did Not Finish       & Did Not Finish         \\ \hline
\end{tabular}
\caption{Table of results for "Follow Person".}
\label{exp_tbl_follow}
\end{table}


\section{Navigate Through Person}\label{exp_sect_person}
\textbf{Scenario:} The robot navigates a corridor with a person blocking the path.

\textbf{Use cases:} Tour guide, shopping mall guide, office navigation

\textbf{Description:} A common issue in the School of Computing; robots often halt navigation when trying to cross the busy and narrow
corridors of the EC Stoner building. Problem is posed in the \acrshort{erl} Consumer Challenge.

\bigskip

\textbf{Implementation:} "Person" Object Handler called on incoming collision detected. The handler asks the person to move aside
and waits until timeout or the planner detects a clear path before continuing navigation.

\bigskip

\textbf{Results:} The mobile robot navigated as per normal when a clear path to the goal was present. As expected, TIAGo engaged in \acrfull{hri} when the path was obstructed by one or more persons, and waited for a path to be cleared before continuing with the navigation goal. The results of this experiment are listed in Table \ref{exp_tbl_through_person}.

\begin{table}[H]
	\footnotesize
	\begin{subtable}[H]{.4\textwidth}
		\centering
		\begin{tabular}{|l|l|l|}
		\hline
		\textbf{Persons} & \textbf{Waited} & \textbf{Goal Reached} \\ \hline
		\multicolumn{3}{|c|}{\textbf{Path Clear}}                  \\ \hline
		0                & N              & Y                      \\ \hline
		1                & N              & Y                      \\ \hline
		2                & N              & Y                      \\ \hline
		\multicolumn{3}{|c|}{\textbf{Path Blocked}}                \\ \hline
		0                & N              & N                      \\ \hline
		1                & N              & N                      \\ \hline
		2                & N              & N                      \\ \hline
		\end{tabular}
		\caption{Control: results with \texttt{move\_base}.}
	\end{subtable}
	\begin{subtable}[H]{.6\textwidth}
		\centering
		\begin{tabular}{|l|l|l|l|}
		\hline
		\textbf{Persons} & \textbf{\acrshort{hri} Attempted} & \textbf{Waited} & \textbf{Goal Reached} \\ \hline
		\multicolumn{4}{|c|}{\textbf{Path Clear}}                                 \\ \hline
		0                & N            & N               & Y                     \\ \hline
		1                & N            & N               & Y                     \\ \hline
		2                & N            & N               & Y                     \\ \hline
		\multicolumn{4}{|c|}{\textbf{Path Blocked}}                               \\ \hline
		0                & N            & N               & N                     \\ \hline
		1                & Y            & \textbf{Y}      & \textbf{Y}            \\ \hline
		2                & Y            & \textbf{Y}      & \textbf{Y}            \\ \hline
		\end{tabular}
		\caption{Results with \texttt{object\_aware\_move\_base}.}
	\end{subtable}
	\caption{Table of results for "Navigate Through Person".}
	\label{exp_tbl_through_person}
\end{table}


\section{Navigate Through Small Object}\label{exp_sect_object}
\textbf{Scenario:} The robot navigates a doorway with a coffee cup blocking the path.

\textbf{Use cases:} Clean-up robot, office navigation

\textbf{Description:} An unaddressed issue in autonomous navigation, robots halt when paths are blocked by small moveable objects.
Problem is posed in the \acrshort{erl} Consumer Challenge.

\bigskip

\textbf{Implementation:} "Coffee" Object Handler called on incoming collision detected. The handler uses Semantic Navigation to approach
1 metre of the object and passes 3D MOT information to the MoveIt motion planning library, picking up the object before continuing navigation.

\bigskip

\textbf{Results:} The robot was seen to remove obstacles in the doorway via manipulation and achieve the navigation goal by crossing into the next room. Qualitative results are shown in Figure \ref{impl_fig_small_obj} and the video demonstration provided in Appendix \ref{appendix_videos}.

\begin{figure}[ht]
	\begin{subfigure}[b]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth, trim=400 100 300 125, clip]{small_object_failure}
		\caption{Control: results with \texttt{move\_base};\\robot is stuck.}
	\end{subfigure}
	\begin{subfigure}[b]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth, trim=400 100 300 125, clip]{small_object}
		\caption{Results with \texttt{object\_aware\_move\_base};\\robot picked up the obstruction.}
	\end{subfigure}
	\caption{"Navigate through small object" Experiment.}
	\label{impl_fig_small_obj}
\end{figure}

\section{Search and Fetch}\label{exp_sect_search}
\textbf{Scenario:} The robot searches a previously mapped room to find and pick up a coffee cup.

\textbf{Use cases:} Care homes, workplace assistants, domestic robots.

\textbf{Description:} An optional objective and extension of Selvaraj's\cite{praveen} project inspired by the "Go Get It!" RoboCup@Home task.
The set-up for this experiment is depicted in Figure \ref{exp_fig_search_setup}.

\bigskip

\begin{figure}[ht]
	\centering
	\includegraphics[width=.75\textwidth, trim=300 100 50 150, clip]{search_and_fetch_0}
	\caption{The WRS test world provided by Toyota\cite{toyota} for the RoboCup@Home 2021 used in "Search and Fetch". Coffee cups are placed in the 5 test locations.}
	\label{exp_fig_search_setup}
\end{figure}

\bigskip

\textbf{Implementation:} \texttt{frontier\_exploration} \cite{frontiers_ros} was configured to match TIAGo's specifications and use a narrow slice of the laser sensor.
The robot explores the room with Semantic Mapping enabled, and performs Semantic Navigation to approach the coffee cup once spotted. A simple grasp
from the top is used with the MoveIt motion planning library to pick it up.

\bigskip

\textbf{Results:}
Table \ref{exp_tbl_search_and_fetch} confirms TIAGo was able to locate the coffee cup in all instances of the experiment, however could not achieve the simple grasp when tested on the highest shelf.
Advanced grasps accounting for position and shape can be used to overcome this limitation.
Added height from torso lift was required to locate the coffee cup on the shelf; this can be overcome by a scanning motion of the head.
Qualitative results can be viewed in Figure \ref{exp_fig_search_and_fetch}.

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{.6\textwidth}
		\includegraphics[width=\textwidth, trim=700 100 300 250, clip]{search_and_fetch_1}
		\caption{Grasp approach pose. MoveIt occupancy grid (intensity gradient) and target collision object (labelled) are shown.}
	\end{subfigure}

	\bigskip

	\begin{subfigure}[b]{.45\textwidth}
		\includegraphics[width=\textwidth, trim=800 100 300 150, clip]{search_and_fetch_2}
		\caption{Area of map re-explored in grey.}
	\end{subfigure}
	\begin{subfigure}[b]{.45\textwidth}
		\includegraphics[width=\textwidth, trim=800 100 300 150, clip]{search_and_fetch_3}
		\caption{Successful search and fetch.}
	\end{subfigure}
	\caption{"Search and Fetch" Experiment.}
	\label{exp_fig_search_and_fetch}
\end{figure}

\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|l|l|l|}
	\hline
	\textbf{Location} & \multicolumn{1}{c|}{\textbf{Torso Lift}} & \textbf{Found} & \textbf{Approached} & \textbf{Picked up} \\ \hline
	Floor             & Default                                  & Y              & Y                   & Y                  \\ \hline
	Long Table        & Default                                  & Y              & Y                   & Y                  \\ \hline
	Tall Table        & Default                                  & Y              & Y                   & Y                  \\ \hline
	Stair-like Shelf  & Default                                  & Y              & Y                   & Y                  \\ \hline
	Shelf             & Maximum                                  & Y              & Y                   & N                  \\ \hline
	\end{tabular}
	\caption{Table of results for "Search and Fetch".}
	\label{exp_tbl_search_and_fetch}
\end{table}

\section{Video Demonstrations}
Videos were recorded of each experiment to demonstrate the performance of Object Aware Navigation with the TIAGo robot.
The videos were sped-up to better convey functionality in each experiment, however it should be noted that recordings were captured at 0.5 to 0.8 of real-time performance,
subject to slow-down from the high-fidelity physics simulation. Links to the video demonstrations, hosted online, can be found in Section \ref{appendix_videos} of the Appendices.