\chapter{Benchmarking}\label{test_chapt}

Benchmarking was performed on the 3D MOT and Semantic Mapping modules of the implementation to test for accuracy and performance.
All tests were run on a single \textit{Intel Core i7-9700K CPU @ 3.60GHz} with GPU acceleration provided by an \textit{NVIDIA GeForce RTX 2060} graphics card.

\bigskip

While these specifications do not match those of TIAGo or similar off-the-shelf mobile robots, computationally expensive processes such as object detection are often offloaded onto separate,
more capable, hardware where data can be communicated over network via ROS or \acrfull{http}.

\section{KITTI Object Tracking Evaluation}\label{test_sect_kitti}
\begin{table}[htbp]
	\centering
	\footnotesize
	\begin{tabular}{|l|l|l|l|l|l|l|l|}
	\hline
	\textbf{Algorithm} & \textbf{sAMOTA} & \textbf{AMOTA} & \textbf{AMOTP} & \textbf{MOTA} & \textbf{MOTP} & \textbf{IDS} & \textbf{\acrshort{fps}} \\ \hline
	SORT (2D)         & 62.66           & 21.68          & 45.84          & 55.96         & 67.46         & 43           & 1439.6       \\ \hline
	Deep SORT (2D)    & 67.74           & 25.35          & 51.34          & 59.36         & 67.70         & 99           & 287.7        \\ \hline
	AB3DMOT        & 74.39           & 29.77          & 53.90          & 69.50         & 78.43         & 0            & 482.7        \\ \hline
	Proposed        & 76.39           & 31.06          & 55.45          & 67.30         & 67.58         & 4            & 65.0         \\ \hline
	\end{tabular}
	\caption{MOT algorithms tested on KITTI Pedestrian benchmark}
	\label{test_tbl_mot_results}
\end{table}

\bigskip

The proposed solution in Chapter \ref{design_chapt} incorporating a deep similarity heuristic into AB3DMOT was tested alongside 
popular MOT algorithms on the KITTI \cite{kitti} MOT Pedestrian validation set.
Table \ref{test_tbl_mot_results} shows the proposed solution having the highest average accuracy (sAMOTA, AMOTA) and precision (AMOTP), though displaying a lower maximum accuracy (MOTA) and precision (MOTP) and a
higher frequency of ID switches (IDS) than its AB3DMOT base. The \acrfull{fps} of all methods meet real-time performance at 30 hertz and above. A full listing of the quantitative results can be found in Table \ref{appendix_tbl_kitti} of the Appendices.

\bigskip

The high average accuracy of the proposed solution means it enjoys better tracking accuracy with less fine-tuning of parameters.
The increase in ID switching over AB3DMOT can be explained by the KITTI benchmark where fully occluded objects are still labelled in the validation set, causing the deep similarity metric to misclassify detections that overlap in the 2D image.
This is reflected in 2D MOT by Deep SORT having a higher IDS
than SORT despite significantly better performance in the MOT Challenge\cite{deep_sort}.

\newpage

\section{Frame Rates}\label{test_sect_framerate}
From the results in Table \ref{test_tbl_framerate} we see a trade-off between maximum speed and scalability in Semantic Mapping pipelines. The Mask R-CNN pipeline has the lowest mean \acrshort{fps}, though scaling better with a significantly lower standard deviation than others. YOLOv3 trades off scalability for speed, with the largest bottleneck being Supervoxel clustering which can be parallelised \cite{lccp}.
YOLACT has the highest standard deviation, likely due to communication overheads between the YOLACT Python 3 node and other ROS-based Python 2 nodes.

\bigskip

\begin{table}[ht]
\centering
\footnotesize
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
\multirow{2}{*}{\textbf{Test No.}} & \multicolumn{3}{c|}{\textbf{Test Parameters}} & \multicolumn{3}{c|}{\textbf{\acrshort{fps}}}		   \\ \cline{2-7}
				  & \textbf{Object Size} & \textbf{Count} & \textbf{Voxel Count} & \textbf{Mask R-CNN} & \textbf{YOLOv3} & \textbf{YOLACT} \\ \hline
1                 & small                & 1              & 1090                 & 6.5                 & 9.4             & 14.3            \\ \hline
2                 & small                & 2              & 2110                 & 6.2                 & 8.5             & 12.0            \\ \hline
3                 & small                & 3              & 3250                 & 5.8                 & 7.6             & 10.5            \\ \hline
4                 & large                & 1              & 5680                 & 6.2                 & 8.6             & 8.0             \\ \hline
5                 & large                & 2              & 11660                & 5.5                 & 7.0             & 5.2             \\ \hline
6                 & large                & 3              & 17420                & 5.1                 & 5.3             & 4.1             \\ \hline
\multicolumn{4}{|c|}{\textbf{Mean}}                                              & \textbf{5.8}        & \textbf{7.7}    & \textbf{9.0}    \\ \hline
\multicolumn{4}{|c|}{\textbf{Standard Deviation}}                                & \textbf{0.5}        & \textbf{1.3}    & \textbf{3.6}    \\ \hline
\end{tabular}
\caption{Semantic Mapping frame rates categorised by pipeline.}
\label{test_tbl_framerate}
\end{table}