\chapter{Implementation}\label{impl_chapt}
\section{Object Detection}\label{impl_sect_detection}
\subsection{YOLOv3}
\acrshort{lasr}'s \texttt{lasr\_object\_detection\_yolo} package, maintained by the author, was readily available and used as the base for the YOLOv3 pipeline proposed in Chapter \ref{design_chapt}.
The package was modified to use a PyTorch implementation of the network \cite{yolov3_pytorch} for easy user access to GPU acceleration. Usage instructions and class definitions can be found in the package README in Appendix \ref{readme_yolo}.


\subsection{Mask R-CNN}\label{impl_subsect_mask_rcnn_det}
A PyTorch implementation of Mask R-CNN is provided in the torchvision library as part of the PyTorch project\cite{pytorch}.
This was chosen as a base for the Mask R-CNN pipeline proposed in Chapter \ref{design_chapt} due to the wide support and ease of use
of the PyTorch framework for Neural Network training and inference with GPU acceleration.

\bigskip

A ROS Service was written hosting the PyTorch implementation of Mask R-CNN to facilitate reusability for other perception-based applications.
Its usage and corresponding InstanceSegmentation ROS service message are defined in the package README included in Appendix \ref{readme_detection}.

\bigskip

The service converts input ROS Images into Python RGB arrays before setting them as input to the Mask R-CNN network and performing a forward pass; the output tensor is organised into an InstanceSegmentation service response. A catkin Python object is also defined that can be imported directly into scripts to avoid communication overheads sending instance segmentation masks over the network with ROS.


\subsection{YOLACT}\label{impl_subsect_yolact}
Integration of the YOLACT instance segmentation framework was one of the optional objectives of the project. A wrapper for the network is provided as an experimental feature in the project, as the official implementation was written in Python 3 - unsupported by ROS Kinetic. The YOLACT node uses the same InstanceSegmentation interface as in the Mask R-CNN node (Section \ref{impl_subsect_mask_rcnn_det}).

\bigskip

The feature is labelled as experimental since network overheads needed to be introduced to communicate between Python 3 and Python 2 ROS nodes. The implementation thus suffers a significant reduction in speed.



\section{3D Segmentation}
\subsection{Locally Convex Connected Patches (LCCP)}
LCCP is performed by clustering an input point cloud into supervoxels, groups of visually and geometrically similar voxels. The supervoxels are clustered further
by breadth-first search, where neighbouring supervoxels at convex angles to each other are assigned the same label.

\begin{figure}[t]
	\centering	
	\includegraphics[trim=400 200 500 200, clip, scale=0.2]{lccp_0}
	\includegraphics[trim=400 200 500 200, clip, scale=0.2]{lccp_1}
	\includegraphics[trim=400 200 500 200, clip, scale=0.2]{lccp_2}
	\caption{LCCP algorithm in simulation.\\Left to right: input point cloud, supervoxel cloud, LCCP clustered cloud}
	\label{impl_fig_lccp_reel}
\end{figure}
\bigskip

LCCP was implemented \cite{lccp} for \acrshort{pcl} 1.7.0, however the TIAGo platform had support up to \acrshort{pcl} 1.6.0. The algorithm was backported to the earlier version for use in
the project, with the requisite header files provided by PCL \cite{pcl} included as part of the project deliverables.

\bigskip

A module that performs LCCP based on an example script available in the \acrshort{pcl} repository \cite{pcl} is exposed as a ROS Service in the \texttt{object\_segmentation\_pcl} package. The service performs the algorithm on an input PointCloud2 using \acrshort{pcl}, outputting labelled and Glasbey \cite{glasbey} coloured point clouds from supervoxel and LCCP clustering as shown in Figure \ref{impl_fig_lccp_reel}.


\subsection{Mask R-CNN}
3D segmentation using Mask R-CNN is fairly straightforward - instance segmentation masks are computed by the network from 2D RGB image projections of point clouds and used to directly index voxels in the original 3D representation. This produces a cluster of voxels for each object segmented from the image as illustrated in Figure \ref{impl_fig_pipeline_mask_reel}.

\subsection{Other Segmentation Services}
A number of useful segmentation services were produced as a by-product of testing candidate solutions to the 3D Instance Segmentation Pipelines.
The services are included with the project deliverables in the \texttt{object\_segmentation\_pcl} package and outlined in the package README in Appendix \ref{readme_chapt}.


\section{3D Instance Segmentation Pipelines}\label{impl_sect_pipelines}
Key stages of the proposed 3D perception solutions are exemplified by the sequences in Figures \ref{impl_fig_pipeline_yolo_reel} and \ref{impl_fig_pipeline_mask_reel}.
Additional pre-processing and post-processing steps were required to obtain satisfactory results from the 3D segmentation methods listed above. Pipelines were written to perform these steps in self-contained C++ functions, reducing overheads from communication of point clouds over the network using ROS.

\bigskip

Note that all voxels in the input cloud are assumed to comprise a single instance extracted using a 2D object detection method. The pipelines perform the following:
\begin{enumerate}
	\item Invalid points (\texttt{NaN/Inf}) are filtered and input cloud is downsampled via VoxelGrid filter.
	\item If YOLOv3, LCCP clustering is performed on the cloud enclosed in a bounding box.
	\item If Mask R-CNN, Euclidean distance clustering is performed on the segmented cloud.
	\item Largest cluster is extracted and transformed to map frame.
	\item 3D bounding box of cluster is computed by maximum and minimum coordinates.
\end{enumerate}


\begin{figure}[t]
	\begin{minipage}{.5\textwidth}
		\centering	
		\includegraphics[trim=750 200 400 200, clip, width=.6\textwidth]{pipeline_input}
		\includegraphics[trim=  0  20   0  70, clip, width=.6\textwidth]{pipeline_yolo_0}
		\includegraphics[trim=750 200 400 200, clip, width=.6\textwidth]{pipeline_yolo_1}
		\includegraphics[trim=750 200 400 200, clip, width=.6\textwidth]{pipeline_yolo_2}
		\caption{YOLOv3 semantic mapping.\\Top to bottom: input cloud, 2D detections, LCCP clustered cloud, 3D MOT boxes}
		\label{impl_fig_pipeline_yolo_reel}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering	
		\includegraphics[trim=750 200 400 200, clip, width=.6\textwidth]{pipeline_input}
		\includegraphics[trim=  0  20   0  70, clip, width=.6\textwidth]{pipeline_mask_0}
		\includegraphics[trim=750 200 400 200, clip, width=.6\textwidth]{pipeline_mask_1}
		\includegraphics[trim=750 200 400 200, clip, width=.6\textwidth]{pipeline_mask_2}
		\caption{Mask R-CNN semantic mapping.\\Top to bottom: input cloud, 2D segmentations, 3D segmentations, 3D MOT boxes}
		\label{impl_fig_pipeline_mask_reel}
	\end{minipage}
\end{figure}


\section{3D Multiple Object Tracking}\label{impl_sect_3d_mot}
The AB3DMOT tracking algorithm proposed in \cite{3d_mot_baseline} was used as a base for the system's 3D tracking capabilities, with the underlying kalman filters modified to store additional
identity information to enable semantic mapping. Further modifications are outlined in this section.

\subsection{Similarity Heuristic}\label{impl_subsect_similarity}
AB3DMOT's update function was modified to accept feature vectors as inputs alongside corresponding 3D detections. The feature vectors are stored in a "gallery" attached to a kalman filter tracking the object's state, later used as an additional identifier for matching incoming detections to tracked identities. This approach was inspired by Deep SORT \cite{deep_sort} for 2D MOT.

\bigskip

The similarity between two feature vectors can be measured by cosine similarity, or the cosine of the angle between them. A cosine similarity matrix between feature vectors of incoming detections
and those stored in the galleries of tracked identities is computed and combined linearly with the \acrfull{iou} matrix to obtain a new similarity matrix.
Equations \ref{impl_eqn_cosine} and \ref{impl_eqn_similarity} define cosine similarity and the proposed similarity heuristic, respectively.

\bigskip

This new similarity matrix is used to find the maximum matching between incoming detections and tracked identities to resolve the updated state of 3D MOT at each time step.

\bigskip

Let $vec_i$ be a feature vector of any object $i$.

Let $G_j$ be a set of feature vectors of a tracked object $j$.

Let $box_i$ be the 3D bounding box of any object $i$.

\begin{equation}\label{impl_eqn_cosine}
cosine(i,j) = min \Bigg\{\frac{vec_i\,^T vec^{(k)}_j}{||vec_i|| \, ||vec^{(k)}_j||} \; \Bigg| \; vec^{(k)}_j \in G_j \Bigg\}
\end{equation}

\begin{equation}\label{impl_eqn_similarity}
similarity(i,j) = \alpha IOU(box_i, box_j) + (1 - \alpha)cosine(i,j), \; 0 \leq \alpha \leq 1
\end{equation}


\subsection{Feature Vectors}
A \acrshort{cnn}-based feature extractor was integrated into the project to demonstrate MOT on the similary heuristic in Section \ref{impl_subsect_similarity}.
The PyTorch implementation of a Residual Neural Network (ResNet)\cite{pytorch,resnet} was trained on the MARS dataset using 
Stochastic Gradient Descent on a triplet loss function $\mathcal{L}$ (Equation \ref{impl_eqn_triplet}) to encode images into feature vectors for use with the new similarity heuristic. This provides feature space projection \cite{facenet, cosine_learning} with easy access to GPU acceleration via the PyTorch library.

\bigskip
Let $A$ be a batch of anchor images with positive matches $P$ and negative matches $N$.

Let $f(X)$ be the result from a forward pass of the ResNet with input $X$.

Let $dist(P1, P2)$ be the Euclidean distance between points $P1$ and $P2$.

\begin{equation}\label{impl_eqn_triplet}
\mathcal{L} = ReLU \Big( dist(f(A), f(P)) - dist(f(A), f(N)) + \alpha \Big)
\end{equation}

\bigskip

By minimising the triplet loss $\mathcal{L}$, images are projected by the ResNet onto a multi-dimensional plane such that for any anchor, positive and negative images, $a, p, n$, respectively,
the Euclidean distance between $f(a), f(p)$ is lower
than that of $f(a), f(n)$. This projection presents a suitable feature vector for the implementation of the cosine similarity heuristic added to AB3DMOT.

\bigskip

The ResNet was trained until it scored an accuracy of 0.7 on the validation set, on par with FaceNet\cite{facenet} and Cosine Metric Learning\cite{cosine_learning}. It was tested with the SORT algorithm on the MOT Challenge\cite{mot_challenge} benchmark and shown to successfully increase precision and accuracy while reducing the number of occurrences of identity switching (Table \ref{appendix_tbl_mot_challenge}, Appendices).
Figure \ref{impl_fig_triplet} shows the cosine similarities between features extracted from a triplet by the trained ResNet.

\begin{figure}
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[height=0.25\textheight]{jeff_1}
		\caption{Anchor image}
	\end{subfigure}
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[height=0.25\textheight]{jeff_2}
		\caption{Positive match}
	\end{subfigure}
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[height=0.25\textheight]{obama}
		\caption{Negative match}
	\end{subfigure}
	\caption{Triplet of images passed through the trained ResNet.\\computed features of (a) and (b) have a cosine similarity of 0.963.\\computed features of (a) and (c) have a cosine similarity of 0.234.}
	\label{impl_fig_triplet}
\end{figure}


\section{Semantic Mapping}
The Semantic Mapping module subscribes to point cloud camera input and performs 2D object detection, 3D segmentation and feature vector computation as described in Sections \ref{impl_sect_detection} to \ref{impl_sect_3d_mot},
with the 3D segmentations used to update an instance of AB3DMOT. The tracked identities output from AB3DMOT are organised into a \texttt{visualization\_msgs/MarkerArray}, a pre-defined ROS message
that can be viewed in RVIZ containing all necessary attributes for 3D tracking identified in Figure \ref{design_fig_tracked_obj_diagram}.

\bigskip

The AB3DMOT outputs are also used to draw the semantic map for \texttt{sensor\_msgs/Image} and \texttt{nav\_msgs/OccupancyGrid} ROS message output. All messages created
are published onto ROS topics for visualisation and use by applications that require 3D MOT.
An example of the 2D and 3D visualisations of semantic mapping can be seen in Figure \ref{impl_fig_semmap}.

\begin{figure}
	\centering
	\begin{subfigure}[b]{.5\textwidth}
		\includegraphics[width=\textwidth, trim=600 300 30 200, clip]{semmap_1}
		\caption{3D Visualisation}
	\end{subfigure}
	\begin{subfigure}[b]{.4\textwidth}
		\includegraphics[width=\textwidth, trim=300 0 0 0, clip]{semmap_0}
		\caption{2D Representation}
	\end{subfigure}
	\caption{Semantic map drawn from 3D MOT output.}
	\label{impl_fig_semmap}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}[b]{.45\textwidth}
		\includegraphics[width=\textwidth]{nms_0}
		\caption{Before \acrshort{nms}; duplicates detected}
	\end{subfigure}
	\begin{subfigure}[b]{.45\textwidth}
		\includegraphics[width=\textwidth]{nms_1}
		\caption{After \acrshort{nms}; duplicates removed}
	\end{subfigure}
	\caption{2D Non-Maximum Suppression on the Semantic Map}
	\label{impl_fig_nms}
\end{figure}

\subsection{Filtering Duplicate Detections}
\acrfull{nms} is performed in 2D on the tracked identities to delete duplicates from the MOT model (Figure \ref{impl_fig_nms}). Tracked identities are sorted in ascending order of their footprint on the map
in an auxiliary list. While the list is not empty, the first identity $i$ is labelled "kept" and removed from the auxiliary list, while any subsequent identities $j$ with a high overlap
with $i$ are deleted. The 2D \acrshort{nms} implementation can easily be expanded to 3D, or disabled via command-line arguments, to allow for semantic mapping in the $Z$-dimension.

\subsection{Removing Absent Identities}
Given the dynamic nature of the problem, it is expected that some objects may move and no longer remain present when part of the map is revisited. The disappearance of such objects should be reflected in the semantic map, thus an algorithm was devised that uses 3D camera information to remove tracked identities found absent from their last seen locations.

\begin{equation}\label{impl_eqn_projection}
\begin{aligned}
	\begin{bmatrix}
	u \\ v \\ w
	\end{bmatrix}
	= P \cdot
	\begin{bmatrix}
	X \\ Y \\ Z \\ 1
	\end{bmatrix}
	\\
	x = u/w
	\\
	y = v/w
\end{aligned}
\end{equation}

3D cameras integrated into ROS carry meta information including a $3 \times 4$ projection matrix $P$ that maps 3D points in the robot's camera frame to 2D pixel coordinates in the camera image. Equation \ref{impl_eqn_projection}\cite{camera_info} is used to obtain the 2D image pixel coordinates $x,y$ from a 3D point $(X,Y,Z)$ in the robot's camera frame given the projection matrix $P$.
Algorithm \ref{impl_algo_deletion} can be executed using Equation \ref{impl_eqn_projection} to remove absent identities from the semantic map.

\begin{algorithm}[H]
	\caption{Removing absent identities}
	\label{impl_algo_deletion}
	\begin{algorithmic}[1]
		\FOR {each tracked identity ID and an integer threshold $t$}
		\STATE get ID's 3D centre point $p$ in map frame
		\STATE query ROS transform buffer to obtain $p$ in robot's camera frame, $(X,Y,Z)$
		\STATE \algorithmicif\ $Z$ is negative \algorithmicthen\ skip ID // \textit{behind the robot; keep}
		\STATE compute 2D image pixel coordinates $x,y$ from $(X,Y,Z)$ via Equation \ref{impl_eqn_projection}
		\STATE \algorithmicif\ $x,y$ are not valid pixel coordinates \algorithmicthen\ skip ID // \textit{not in view; keep}
		\STATE \algorithmicif\ ID was updated in last $t$ frames \algorithmicthen\ skip ID // \textit{in view and detected; keep}
		\STATE delete ID // \textit{in view but not detected; remove}
		\ENDFOR
	\end{algorithmic}
\end{algorithm}

An additional step between lines 7 and 8 of Algorithm \ref{impl_algo_deletion} takes a sample of the depth values around $x,y$ in the corresponding 3D point cloud. If the sampled depth is shallower than $Z$ by some distance $d$ or more, it is assumed the current ID is occluded and should not be deleted.

\bigskip

The implementation of Algorithm \ref{impl_algo_deletion} ensures any tracked identities expected to be in view are deleted on remaining undetected,
preventing an excess of missing objects tracked that could render the semantic map highly inaccurate. The effect of removal is depicted in Figure \ref{impl_fig_clearing}.

\begin{figure}
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[trim=700 150 400 250, clip, width=\textwidth]{clearing_1}
		\caption{Set-up: 7 people tracked on the semantic map}
	\end{subfigure}
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[trim=700 150 400 250, clip, width=\textwidth]{clearing_2}
		\caption{6 people absent\\(without deletion)}
	\end{subfigure}
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[trim=700 150 400 250, clip, width=\textwidth]{clearing_3}
		\caption{6 people absent\\(with deletion)}
	\end{subfigure}
	\caption{Absent identities on the semantic map with and without deletion.}
	\label{impl_fig_clearing}
\end{figure}

\section{Semantic Navigation}\label{impl_sect_semnav}
A python class was written that provides convenience functions to navigate to and look at tracked identities on the semantic map. Movement in particular involves sampling points
in a circle around the object and using \texttt{move\_base} to navigate to the nearest point where a path is successfully found. An example of this process is depicted in Figure \ref{impl_fig_semnav}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.75\textwidth, trim=550 250 550 250, clip]{semnav}
	\caption{TIAGo navigating to the closest reachable point around a person.}
	\label{impl_fig_semnav}
\end{figure}



\section{Object Aware Navigation}\label{impl_sect_oanav}
\subsection{Modified 2D Cost Map}
\texttt{move\_base} provides autonomous navigation to robots by planning paths to a given point on the map. This is achieved by performing a path search algorithm (Dijkstra's or A*) on a global cost map, with the aim of finding a path to the goal that minimises total cost.

\bigskip

Under normal circumstances, obstacles detected via the robot's sensors are projected onto the 2D cost map with infinite cost to ensure mobile robots plan around them during navigation.
\texttt{costmap\_2d} was modified such that each obstacle layer of the 2D cost map listens to a topic
for semantic map information, where coordinates that coincide with a tracked identity on the semantic map have their cost set to a value less than infinity (Figure \ref{impl_fig_costmap}).

\bigskip

This allows mobile robots to plan through objects present on both the global cost and semantic maps, subject to some cost, proven in Figure \ref{impl_fig_global_planner}. Each layer of the cost map has a dedicated topic to listen for tracked identities,
meaning local obstacle layers that handle collision avoidance can be excluded from this communication to retain the original infinite costs. This is demonstrated in Figure \ref{impl_fig_costmap} where infinite local cost is still present at the tracked identity.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.75\textwidth, trim=550 50 350 550, clip]{costmap}
	\caption{Modified 2D cost map; areas highlighted in blue are of some cost while cyan denotes a lethal obstacle. Obstacles in the local cost map (collision avoidance) are displayed in magenta.}
	\label{impl_fig_costmap}
\end{figure}

\subsection{Object Aware Move Base}\label{impl_subsect_oamovebase}
The Object Aware Move Base server accepts navigation goals to be piped to ROS \texttt{move\_base} while monitoring the semantic map and
current navigation path.

\bigskip

Potential collisions are detected by sampling points along a computed navigation path and testing if they lie within the 2D bounding box of any tracked identities.
If the server detects a collision between a tracked object and the robot within a set distance along the current path, navigation is temporarily halted
and control is passed to an "Object Aware Handler" to interact with the obstruction. This process is depicted in Figure \ref{impl_fig_object_aware_smach}.

\bigskip

Object Aware Handlers form the basis of Object Aware Navigation, enabling mobile robots to perform user-defined actions when a tracked object is obstructing its path. Each action
will be exposed as an \texttt{actionlib} server on a ROS topic indicating the object it handles, called using the \texttt{HandleObject} message indicating the tracked identity
to be handled and an error code on return. The ROS computation graph in Figure \ref{design_fig_move_base_graph} illustrates the one-to-many communication.

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.6]{object_aware_sm}
	\caption{State machine diagram of the Object Aware Move Base server.}
	\label{impl_fig_object_aware_smach}
\end{figure}


\section{Object Aware Handlers}
Two object aware handlers are included as deliverables for this project. New handlers can be implemented by users to interact with a wider range of objects.

\subsection{Person Handler}
The handler executes the following when on a collision course with a person:

\begin{enumerate}
	\item Cancel and store the navigation goal
	\item Ask the person to move aside
	\item Wait for the path to be clear
	\item Re-send the stored navigation goal
\end{enumerate}

The current implementation uses a text log to demonstrate \acrfull{hri}, however many off-the-shelf robots including TIAGo support built-in text-to-speech functionalities which can easily be migrated to. The \texttt{move\_base} planning service \cite{move_base} is used to generate paths (Figure \ref{impl_fig_global_planner}) to the navigation goal on which collision detection, described in Section \ref{impl_subsect_oamovebase}, is performed to test for clearance.

\begin{figure}[ht]
	\centering

	\includegraphics[width=0.9\textwidth, trim=450 150 25 450, clip]{planner}
	\caption{Global planner creating a path through a tracked person.}
	\label{impl_fig_global_planner}
\end{figure}



\subsection{Small Object Handler}
The handler executes the following when on a collision course with a small object:

\begin{enumerate}
	\item Cancel and store the navigation goal
	\item Approach 1 metre of the target
	\item Look at the target
	\item Pick up the target with MoveIt
	\item Re-send the stored navigation goal
\end{enumerate}

Semantic Navigation detailed in \ref{impl_sect_semnav} is used to approach and look at the object. The 3D bounding boxes produced by MOT translate directly into MoveIt collision objects (Figure \ref{impl_fig_moveit}) and a simple grasp approaching from the top was implemented to pick them up. This can be extended to use complex grasp generation methods that operate on non-uniform point clouds.

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{.45\textwidth}
		\includegraphics[width=\textwidth, trim=775 350 750 250, clip]{moveit_0}
		\caption{Detected coffee cup as a 3D MOT tracked identity to be passed to MoveIt.}
	\end{subfigure}
	\begin{subfigure}[b]{.45\textwidth}
		\includegraphics[width=\textwidth, trim=775 350 750 250, clip]{moveit_1}
		\caption{Detected coffee cup as a MoveIt collision object in the occupancy grid.}
	\end{subfigure}
	\caption{Semantic Map compatibility with MoveIt.}
	\label{impl_fig_moveit}
\end{figure}