\chapter{Background Research}
\label{research_chapt}

\section{Problem Statement}
Observations in the University of Leeds alone motivate the need for object-aware navigation. Hallways in the EC Stoner building span as little as 2 metres wide and consist of numerous doorways, presenting a challenge when using the ROS navigation stack.

\bigskip

The distance from obstacles within which the robot should not enter, or inflation radius, is set to 0.55 metres by default in the TIAGo robot's ROS navigation stack. This means that for a 2-metre-access-way or 1-metre-doorway with a person standing directly in the centre, the inflation radius would occupy the entire width, often causing the robot to plan a significant detour or halt navigation altogether when trying to cross their path.

\bigskip

This is not desired behaviour, as an intelligent system should attempt to interact with known objects in order to create the optimal solution, which in this case would be to kindly ask the person to move aside such that the robot can navigate the corridor.

\bigskip
\noindent
\textbf{RoboCup@Home, \acrshort{erl} and SciRoc:}
Object-aware navigation is a benchmark functionality required of the RoboCup@Home league and \acrfull{erl}. This is highlighted in the "Carry my Luggage" scenario of the RoboCup@Home\cite{robocup_2020}, where the robot is tasked with following a person and navigating social constructs such as barriers, while "Visit my home" of the \acrshort{erl} Consumer challenge\cite{erl_consumer} tasks the robot with navigating a room by moving obstructing objects and interacting with human obstacles to reach the destination. The SciRoc Challenge\cite{sciroc} poses an autonomous waiter scenario where object-awareness is required to determine if a table is waiting to be served based on the presence of people and menu items.

This justifies the need for object-aware navigation in robotics research, where it is currently an open problem used as a benchmark in competitions at an international level.

\section{ROS Ecosystem}\label{sect_research_ros}
The Robot Operating System (ROS)\cite{ros} is an open-source middleware platform that provides design conventions, libraries and communication services that abstract software and hardware implementations from the process of creating complex robot software. Useful open-source technologies that support ROS will be described in this section.

\bigskip
\noindent
\textbf{Robot Operating System:}
%ROS is a software platform that provides high-level abstractions of software and hardware components present in most modern Operating Systems, designed in the context of general-purpose robot development\cite{ros}.
Nodes\cite{ros_guide} are core processes run by ROS, where many nodes can be run synchronously or asynchronously and communicate via a network-based message passing service, enabling modularity and abstraction in system design.
\texttt{actionlib} builds a client-server architecture on top of ROS nodes to perform long-running tasks.
%This enables modularity and abstraction in system design, where nodes of varying complexity are run concurrently on multiple machines, ranging from low-level motor control on the robot to high-level behavioural planning on a network-connected server.

%roscpp and rospy libraries provided by ROS allow developers to write nodes in C++ or Python, as well as implement specialised nodes such as Services and Action Servers. These specialised nodes introduce additional features to readily perform frequently-used or long-running tasks, running efficiently as background processes when not in use.

Software developed for ROS is managed by a catkin workspace, which handles the compilation and exportation of software packages for use by the system. Functionality can thus be easily shared between different modules in the workspace, promoting object-oriented design and rapid prototyping of software in the project by creating packages.

%These base features of ROS, explained in greater detail in \cite{ros_guide}, will be used to their full advantage in the project to implement a modular and computationally efficient system.

\bigskip
\noindent
\textbf{Navigation stack:}
ROS supports a modular 2D navigation stack\cite{ros_nav_stack} that uses sensor inputs to build a map of the surrounding environment, plan paths between different poses on the map, and calculate safe trajectories to send to the robot's mobile base. %Packages included in the navigation stack will be extended to enable the functionality required by the project.

The \texttt{costmap\_2d} package\cite{costmap_2d} implements a layered 2D occupancy grid for the navigation stack. This is primarily used to label obstacles detected by depth sensors as weighted points on the map, creating heat maps in the inflation layer of the cost map indicating safe proximities to approach them. To enable object-aware path planning, the obstacle layer was modified to prevent tracked objects from being labelled as impassable obstructions.

The \texttt{move\_base} package\cite{move_base} implements a ROS \texttt{actionlib} server that handles the full-stack navigation of a robot from a start point to an end goal on a 2D map. This includes the instantiation of 2D costmaps and path planners used in calculating safe velocities to send to the mobile base controller. The handling of tracked objects was implemented at this level, where the intricacies of path planning and velocities are abstracted away.

%The navigation stack is a useful collection of packages that will be modified to meet the aims of the project, where the modular design of ROS and catkin workspaces mean that the deliverables can easily be integrated alongside other versions of the same software.

\bigskip
\noindent
\textbf{Point Cloud Library:}
%The \acrfull{pcl} is highly integrated with ROS\cite{pcl_ros}, where roscpp and rospy provide modules to interpret and process Point Clouds, as well as convert freely between \acrshort{pcl} and ROS datatypes.
Point clouds are data structures that represent images in multi-dimensional spaces. \acrfull{pcl}\cite{pcl} is highly integrated with ROS, providing an extensive C++ library of functions to augment, filter and cluster 3D point clouds generated from \acrfull{rgbd} images. The library proved extremely useful in segmenting objects and creating occupancy grids for an object-aware system. 
%An example can be seen in \cite{momo} where unknown objects are detected from a scene by filtering planes detected in the Point Cloud and returning Euclidean clusters.

\bigskip
\noindent
\textbf{MoveIt:}
MoveIt\cite{moveit} is a framework that implements algorithms for motion planning, collision detection and inverse kinematics that allow users to move effectors of a robot to a desired configuration and manipulate objects using a high-level interface. This was used to craft experiments that involved the grasping of tracked objects.

\bigskip
\noindent
\textbf{Gazebo and RVIZ:}
Gazebo\cite{gazebo} is a high-fidelity physics simulator for conducting experiments on robots, while RVIZ\cite{rviz} is a graphical visualisation tool for ROS that allows users to monitor sensor inputs and the state of the robot. These tools were used extensively for testing solutions throughout the course of the project, mitigating risks of hardware damage and limited access to the robot.

\bigskip
\noindent
\textbf{Frontier Exploration:} Yamauchi\cite{frontiers} introduces an approach for autonomous exploration using frontiers. An occupancy grid is initialised with probabilities at each cell, where cells with values equal to, lower than or higher than the initial values correspond to unknown, open and occupied space, respectively. Frontiers are boundaries between open and unknown space, and are detected using an algorithm similar to edge detection in computer vision. The robot navigates to these boundaries until no further frontiers are detected on the grid. Frontier Exploration was implemented for ROS by Paul Bovbel\cite{frontiers_ros}.


\section{The TIAGo Robot}
The TIAGo robot is an advanced hardware platform manufactured by PAL Robotics targeting research in service robotics. The university's TIAGo model is equipped with a 0.6-8m range \acrshort{rgbd} camera mounted on a pan-tilt head, a 7 Degrees of Freedom arm, a parallel gripper, and a mobile base with a 10m range laser\cite{tiago}. The robot is readily integrated with perception, navigation and motion planning libraries for use with ROS.


\section{Object Detection and Segmentation}
Object detection and segmentation from input depth images was required to build a semantic map of the robot's surroundings for object-aware navigation. Candidate methods that implement this functionality are listed below:

\bigskip
\noindent
\textbf{You Only Look Once (YOLO):}
%\subsection{You Only Look Once: Unified, Real-Time Object Detection}
Redmon et al.\cite{yolo} present a deep \acrfull{cnn} that is trained, given an image, to predict bounding box coordinates, as well as an "objectness" score and classification scores for each box. This is an end-to-end solution for object detection that has a high mean average precision and framerate, making the architecture extremely useful for robot perception where fast and accurate information is needed for decision-making.

\bigskip
\noindent
\textbf{Mask R-CNN:} He et al.\cite{mask-rcnn} propose an extension to the two-stage Faster R-CNN framework for object detection. They add a branch of computation that applies a \acrfull{fcn} to each \acrfull{roi} proposed in the first stage of the neural network to predict a pixel-wise segmentation mask. The \acrshort{fcn} runs in parallel to the second stage of the network which performs bounding box regression and classification on each \acrshort{roi}. The method outperformed all previous results on the Microsoft COCO \cite{coco} dataset's image segmentation task at the time of its inception, and has significant value in 3D perception where objects can be segmented directly from the 2D projection of a point cloud.

\bigskip
\noindent
\textbf{YOLACT:} Bolya et al. \cite{yolact} achieve real-time instance segmentation through task parallelism to create a one-stage approach to the problem. YOLACT uses a standard feature backbone followed by two branches of computation for instance segmentation. The first uses \acrshort{fcn}s to compute $k$ prototype masks that learn to extract backgrounds, foregrounds and features of objects, while the second regresses bounding boxes, object and class scores as well as $k$ coefficients for each object. The coefficients and prototypes are linearly combined to produce segmentation masks, while bounding boxes crop them out of the image to produce instance segmentations.
YOLACT runs at speeds upwards of 30 \acrfull{fps}.

\bigskip
\noindent
\textbf{6D Pose Estimation:}
%\subsection{PoseCNN: A Convolutional Neural Network for 6D Object Pose Estimation In Cluttered Scenes}
Xiang et al.\cite{posecnn} introduce PoseCNN, a \acrshort{cnn} with 3 branches of classification and regression to predict segmentation, depths and object centres to produce 6D poses (X, Y, Z, Roll, Pitch, Yaw) of known objects in an input image. Tremblay et al.\cite{dope} propose a related work, DOPE, where a \acrshort{cnn} branches to predict a belief map of 3D bounding box vertices and their vectors from a centre point, before a greedy algorithm is used to search the maps for 6D objects poses in an image. It is trained on photorealistic data generated randomly using 3D models of known objects.

While both methods are extremely useful in perceiving objects for robot manipulation, they require 3D dimensions of objects to be known for training, reducing generalisation of the solution to objects in the same class that exhibit different physical properties.

\bigskip
\noindent
\textbf{Locally Convex Connected Patches (LCCP):} Stein et al.\cite{lccp} present a model-free algorithm for segmenting 3D point clouds obtained from \acrshort{rgbd} input. They cluster the cloud into supervoxels, groups of 3D points (voxels) with similar properties, and for each pair calculate the angles from their normals to the vector between them, determining if they are convex or concave. Then, breadth-first search is used to cluster convex neighbouring supervoxels together until the entire point cloud has been labelled. This creates high quality segmentations without the use of machine learning, based on psychophysical studies which show that separation in images can be perceived by the convex patches they are comprised of.

\bigskip
6D pose estimation shows great promise for the future of robot perception, however current state of the art methods require exact 3D dimensions of known objects for training, limiting generalisation and scalability. This does not meet the requirements of the project, where many objects of the same class can vary greatly in appearance, shape and size, especially in the case of people. Hence, 2D detection methods were used with Point Cloud segmentation to approximate such performance, where Mask R-CNN and LCCP produce high-quality segmentations fit for the purpose of determining spatial occupancy.


\section{Multiple Object Tracking}
%Object tracking algorithms have been a significant focus of computer vision research, with many algorithms developed to handle multiple object tracking (MOT) in 2D and 3D.

3D multiple object tracking (MOT) is a key requirement of object-aware navigation, as the robot needs to know the state and trajectory of objects in its surroundings.

\bigskip
\noindent
\textbf{Deep SORT:}
Wojke et al.\cite{deep_sort} implement a simple extension to the SORT\cite{sort} algorithm for 2D object tracking, using a Kalman Filter to estimate tracked object states, and the Hungarian algorithm on the \acrfull{iou} of bounding boxes with an added person re-identification score to match incoming detections with previously tracked objects. Deep SORT shows high performance in the MOT Challenge, with over 90\% accuracy on the test set.

\bigskip
\noindent
\textbf{3D Multi-Object Tracking:}
%\subsection{3D Multi-Object Tracking: A Baseline and New Evaluation Metrics}
Weng et al.\cite{3d_mot_baseline} propose a 3D MOT evaluation toolkit, along with a baseline 3D
MOT tracker based on SORT\cite{sort} that utilises Kalman filters to estimate object states, and the Hungarian algorithm to match new detections to tracked objects. Detections are passed to the tracker as 3D bounding boxes.

\bigskip
\noindent
\textbf{Person Re-identification:} Schroff et al.\cite{facenet} and Wojke et al.\cite{cosine_learning} use triplet loss and cosine metric loss functions, respectively, with \acrshort{cnn}s to learn feature spaces onto which images can be projected. The Euclidean and cosine distances between pairs of feature vectors computed from input images can be used to measure their similarity and thus re-identify previously seen individuals. Person re-identification has been integrated into MOT algorithms such as SORT \cite{sort, deep_sort} to improve accuracy and reduce identity switching during tracking.

\bigskip
3D MOT is a relatively novel area of research, where recent breakthroughs in computer vision are enabling rapid progress in the field. As such, existing technology in 2D MOT was leveraged for the project, where the aforementioned baseline 3D MOT method was modified to integrate person re-identification as in Deep SORT.


\section{Related Works}
No works were found that integrated MOT into the navigation stack during the literature review process, however related methods for semantic mapping and socially-aware navigation were researched that rely on the perception of objects for robot navigation.

\bigskip
\noindent
\textbf{Object-Aware Hybrid Map:}
%\subsection{Object-Aware Hybrid Map for Indoor Robot Visual Semantic Navigation}
Wang et al.\cite{semantic_map_supervoxel} propose a method for semantic mapping that allows for intelligent navigation by robots. They use LCCP to obtain 3D segmentations of objects detected from point clouds using YOLOv3\cite{yolov3}, with euclidean distance matching and computed Manhattan frames to fuse segmentations of objects from multiple angles. This produces accurate spatial representations of objects used to generate a 3D occupancy grid storing semantic information. They indicate successful experiments with a robot tasked to navigate to named objects in the environment by speech commands.

\bigskip
\noindent
\textbf{QuadricSLAM:}
%\subsection{QuadricSLAM: Dual Quadrics from Object Detections as Landmarks in Object-oriented \acrshort{slam}}
Nicholson et al.\cite{quadric_slam} define a parameterised form of dual quadrics used to perform \acrshort{slam} with object detection frameworks as a sensor. They propose that any bounding box can be said to contain all or part of a conic, and present a means of generating 3D quadrics from bounding boxes supplied by modern object detection systems. They further define an error function on odometry data and the quadric information that can be minimised by a localisation system. This enables \acrshort{slam} to use object detection outputs directly as sensors, where localisation depends on meaningful semantic 3D quadric landmarks as opposed to geometric information obtained from laser sensors.

%\bigskip
%\noindent
%\textbf{Adaptive Human aware Navigation based on Motion Pattern Analysis:}
%Hansen, S.T. et al. implemented a system\cite{human_nav} that learns to predict if a person wants to interact with the robot using Case Based Reasoning, given their estimated pose, and calculates the most natural approach to the person based on psychological studies.

\bigskip
\noindent
\textbf{Socially-Aware Navigation:}
Significant progress has been made in socially-aware navigation in the context of humans, where autonomous robots are able to plan socially acceptable paths when navigating in close proximity to them\cite{human_nav, social_nav, social_nav2}. Banisetty et al.\cite{social_nav} in particular implement a local planner for the ROS Navigation Stack that defines social features and corresponding cost functions to be optimised during path planning.


\bigskip

Although the methods described above aim to solve different problems in mobile robot navigation, they all highlight a need for object-aware systems in modern robotics. This point is furthered by Nicholson et al.\cite{quadric_slam}, stating a lack of adoption of recent advancements in computer vision by the \acrshort{slam} community.

These works also demonstrate significant background research in object perception and navigation, where similar approaches were used to improve the performance of the navigation system to be developed. Methods described by Wang et al.\cite{semantic_map_supervoxel} were particularly useful, since 3D semantic mapping of the environment was essential for object-aware path planning.

\section{Previous Work at the University of Leeds}
Mohammed Alshammasi\cite{momo} presented a solution to the cleanup task of the RoboCup@Home league\cite{robocup_2020}, where a robot was able to segment objects from the scene and navigate towards them using pre-defined points before picking them up.

Praveen Selvaraj\cite{praveen} developed an extension to Alshammasi's work where the robot accepts instructions via speech to search for an object in the environment, either reporting its location to the user or using the robot arm to pick it up and bring it back.

The background research and ideas presented by these works were leveraged in this project, as they detail challenges faced working with 3D object perception and the TIAGo robot under similar project conditions.


\section{Datasets}
The following datasets were found that can be used to test and improve the performance of modules developed for the project:

\bigskip

\textbf{MOT Benchmarks:} The MOT Challenge by Leal-Taixé et al.\cite{mot_challenge} and AB3DMOT by Weng et al.\cite{3d_mot_baseline} aim to standardise the benchmarking of MOT methods. Leal-Taixé et al. provide a large 2D object tracking dataset for validation, while AB3DMOT is based on the KITTI Object Tracking Evaluation dataset by Geiger et al.\cite{kitti}; both collated by trusted sources. The benchmarks and corresponding datasets can be used to effectively test the project's MOT implementation and produce quantitative results for comparison with other methods.

\bigskip

\textbf{Motion Analysis and Re-identification Set (MARS):} This dataset by Zheng et al. is an extension of their Market1501 dataset that aims to benchmark large scale person re-identification\cite{mars}. It comes from a trusted source as a collaboration between Tsinghua University,
Microsoft Research, UTSA and Peking University.
%while the Market1501 dataset was used to train open-source models offered through Intel's OpenVINO toolkit\cite{openvino}.
MARS was used to train a similarity metric for increased performance in Deep SORT\cite{deep_sort}, yielding similar results when paired with 3D MOT.

\bigskip

\textbf{NYU Depth V2:} A large dataset from a trusted source, Silberman et al. at New York University, featuring labelled
depth images of indoor scenes\cite{nyu_depth}. The images enable the testing of object segmentation modules developed for the
project in varied environments that would otherwise have been impossible amidst restrictions brought about by COVID-19.


\section{Previous Work by the Author}
The author is a member of the \acrfull{lasr} robotics team headed by Dr. Matteo Leonetti, and a maintainer of their code base hosted on the sensible-robots GitLab group. The author developed the following ROS packages available for use by team members and Final Year Undergraduate Project students:
\begin{itemize}
	\item \texttt{lasr\_object\_detection\_yolo} object detection modules including a Costa dataset with pre-trained YOLOv3 models
		for the SciRoc Challenge 2019
	\item \texttt{jeff\_segment\_objects} detects unknown objects using \acrshort{pcl}
	\item \texttt{jeff\_object\_tracking} MOT and a Deep SORT PyTorch port for person following
	\item \texttt{lasr\_sciroc} in collaboration with Mohammed Alshammasi and Logan Dunbar to handle the coffee shop scenario
		at the SciRoc Challenge 2019
	\item \texttt{robocup\_receptionist} in collaboration with Haoran Li to perform the receptionist task at the RoboCup@Home Education
		Online Challenge 2020
\end{itemize}