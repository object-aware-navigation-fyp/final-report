\chapter{Introduction}
\label{intro_chapt}
Autonomous robots have seen rapid adoption in society over the last decade following major advancements in Deep Learning for visual and auditory perception, leaving the confines of factories and research labs to perform complicated tasks in real-world situations. Activities such as driving, goods delivery and cleaning are increasingly being taken over by autonomous systems with the ability to navigate complex unstructured environments designed for human traversal\cite{tesla_autopilot,starship,cleaning_robots}.

\bigskip

Such behaviour requires an intelligent understanding of a robot's surroundings in order to navigate physical obstructions and social contructs present in society. To tackle this problem, researchers have developed \acrfull{slam} for autonomous navigation and Multiple Object Tracking (MOT) to predict the state and trajectory of nearby objects given visual and spatial input.

\bigskip

However, currently available \acrshort{slam} systems consider any entity in front of the robot, including objects tracked visually by MOT, as an obstacle, causing mobile robots to detour around them or even stop navigation altogether when an alternative path cannot be found. This has been highlighted in many major robotics challenges, where benchmark tasks are set that involve the navigation of people, moveable furniture and small objects blocking the only path to the goal.

\bigskip

A number of works have been published on socially-aware navigation with humans\cite{human_nav,social_nav,social_nav2}, however few generalise to navigate moveable inanimate objects present in the environment. This project aims to bridge that gap by developing an object-aware navigation system that is able to store the 3D state of each object in a robot's surroundings and autonomously execute actions to open up new paths in the world by interacting with them.

\section{Aims}\label{intro_sect_aims}
The aim of this project is to produce a simple, robust and platform-independent solution to object-aware navigation, using 3D camera input to segment and track objects in real-world coordinates while providing a high-level interface to perform actions such as object manipulation and human interaction when navigating tracked identities.

\bigskip

A number of experiments were designed for the TIAGo robot around benchmark navigation tasks presented by the RoboCup@Home league and \acrfull{erl} to test the proposed system's efficacy, where scripts developed to conduct the experiments can be repurposed for use in the actual competitions.

\newpage

\section{Objectives}
The problem presented in section \ref{intro_sect_aims} was broken down into 5 sub-tasks, namely object segmentation, multiple object tracking, semantic mapping, object-aware navigation and experimentation. The first 4 sub-tasks were key components of the system, while experimentation was seen as a form of acceptance testing to produce a complete solution.

\bigskip

Objectives of the project derived from these sub-tasks are given below, where the "Must have", "Should have", "Could have" and "Won't have" (MoSCoW method) categories were used to prioritise requirements.

\subsection{Minimum Objectives}\label{intro_subsect_min_objectives}
"Must have" and "Should have" requirements are listed as minimum objectives:
\begin{itemize}
	\item Platform-independent object-aware navigation ROS package
	\item Object detection and 3D segmentation services
	\item Robust 3D object tracking system
	\item Semantic mapping and navigation system
	\item Easy-to-use interface promoting further use in the university
	\item Good performance on benchmark tasks for participation in competitions
\end{itemize}

\subsection{Optional Objectives}\label{intro_subsect_optional_objectives}
"Could have" requirements are listed as optional objectives:
\begin{itemize}
	\item Real-world testing where safe and possible amidst the COVID-19 pandemic
	\item Object retrieval software extending "Robotic Search and Fetch"\cite{praveen}
	\item Integration of YOLACT\cite{yolact} for real-time Instance Segmentation
\end{itemize}



\section{Deliverables}
\begin{enumerate}
	\item Final report detailing development and evaluation of the project
	\item ROS packages implementing 3D perception and object-aware navigation
	\item Python scripts utilising object aware navigation to solve the
		RoboCup@Home "Carry my luggage" and "Go Get It!" tests, as well as the \acrshort{erl} "Visit my home" task
	\item Neural Network weights for person re-identification
	\item Video demonstrations of object-aware navigation with the TIAGo robot
	\item GitLab READMEs with installation and usage instructions
\end{enumerate}
